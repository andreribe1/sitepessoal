#!/bin/bash

diretorio_atual=$2
# cd /var/www/html/ecidade-sete-lagoas
dirarq=$(pwd)
cd $diretorio_atual
echo $dirarq
echo $diretorio_atual

initial_file="$1"  # Arquivo inicial para pesquisa
graph=()  # Lista vazia para armazenar as arestas do grafo
listaw=()

# Função recursiva para pesquisar os arquivos que chamam o arquivo inicial
function search_callers {
  local file="$1"

  # Verificar se o arquivo já foi processado para evitar ciclos
  if [[ " ${graph[*]} " =~ " $file " ]]; then
    return
  fi
  # for item in "${lista[@]}"; do
  #     if [[ "$item" == "$file" ]]; then
  #         tem=false
  #     fi
  # done
  # if [[ tem ]]; then
  #     tem=false
      listaw+=("$file")
  # fi

  # Adicionar o arquivo ao grafo
  graph+=("$file")

  # Pesquisar os arquivos que chamam o arquivo atual
  callers=$(grep -lr --include='*.php' --include='*.js' --exclude-dir='extension' "$file" . )

  # Verificar se existem arquivos que chamam o arquivo atual
  if [ -n "$callers" ]; then
    # Percorrer os arquivos que chamam e chamar a função recursivamente
    while IFS= read -r caller; do
      search_callers "$caller"
      # Adicionar aresta ao grafo
      graph+=("$caller -> $file")
      # echo "> $file <"
        if [[ " ${lista[*]} " =~ " $file " ]]; then
            lista+=("$file")
        fi

    done <<< "$callers"
  fi
}

# Verificar se o arquivo inicial existe
if [ ! -f "$initial_file" ]; then
  echo "O arquivo '$initial_file' não existe."
  exit 1
fi

# Chamar a função para pesquisar os arquivos que chamam o arquivo inicial
search_callers "$initial_file"
# Imprimir o grafo
echo "${listaw[*]}"
echo ""> "$dirarq/lista.txt"
echo "$dirarq/lista.txt"
for edge in "${listaw[@]}"; do
    edge=${edge#./} 
  echo "$edge" >> "$dirarq/lista.txt"
done



