function downloadArquivo() {
    // Seu conte�do do arquivo

    // Criar um link de download
    const linkDownload = document.createElement('a');
    linkDownload.href = url;
    linkDownload.download = 'arquivo.txt'; // Nome do arquivo
    linkDownload.textContent = 'Clique aqui para baixar o arquivo';

    // Adicionar o link ao corpo do documento (opcional)
    document.body.appendChild(linkDownload);

    // Clicar no link automaticamente
    linkDownload.click();

    // Remover o link do corpo do documento (opcional)
    document.body.removeChild(linkDownload);

    // Revogar o URL para liberar recursos
    URL.revokeObjectURL(url);
}