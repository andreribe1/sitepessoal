#!/bin/bash

# Instalar pacotes necess�rios
sudo apt-get install -y libqt5sql5 libqt5sql5-psql

# Configurar vari�vel de ambiente
export QT_PLUGIN_PATH=/usr/lib/qt/plugins

# Obter o diret�rio home do usu�rio em execu��o
DIRETORIO_HOME=$HOME

# Instalar os arquivos no diret�rio home do usu�rio
sudo cp -r /usr/local/Mapeador $HOME

sudo chmod 755 -R "$HOME/Mapeador"