#!/bin/bash

# Verifique se o número de argumentos é válido
if [ $# -ne 2 ]; then
    echo "Uso: $0 <arquivo> <diretório>"
    exit 1
fi

arquivo=$1
dir=$2
raiz=$(pwd)

# Verifique se o diretório especificado existe
if [ ! -d "$dir" ]; then
    echo "Diretório '$dir' não encontrado."
    exit 1
fi

# # Remova o arquivo plugin.txt se ele existir
# if [ -e "$raiz/plugin.txt" ]; then
#     rm "$raiz/plugin.txt"
# fi

cd "$dir" || exit 1

# Encontre os arquivos XML e armazene os nomes no arquivo plugin.txt
find . -name "*.xml" -exec grep -il "$arquivo" {} \; -printf "%f\n" > "$raiz/plugin.txt"
